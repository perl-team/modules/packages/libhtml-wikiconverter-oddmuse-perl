libhtml-wikiconverter-oddmuse-perl (0.52-3) unstable; urgency=medium

  [ Xavier Guimard ]
  * Email change: Xavier Guimard -> yadd@debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already present in machine-readable debian/copyright).
  * Update standards version to 4.2.1, no changes needed.
  * Bump debhelper from old 12 to 13.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 12 Dec 2022 22:37:19 +0000

libhtml-wikiconverter-oddmuse-perl (0.52-2) unstable; urgency=low

  [ Jonas Smedegaard ]
  * Depend on ${misc:Depends}.
  * Drop bogus dependency on liburi-perl.

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).

  [ Salvatore Bonaccorso ]
  * debian/control: Changed: Replace versioned (build-)dependency on
    perl (>= 5.6.0-{12,16}) with an unversioned dependency on perl (as
    permitted by Debian Policy 3.8.3).

  [ gregor herrmann ]
  * Set Standards-Version to 3.9.1; replace Conflicts with Breaks.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Update Vcs-Browser URL to cgit web frontend
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/*: update URLs from {search,www}.cpan.org to MetaCPAN.

  [ Xavier Guimard ]
  * Switch from cdbs to dh:
    - remove cdbs files
    - remove cdbs dependency
    - switch debian/rules to dh
  * Bump debhelper compatibility to 10
  * Declare compliance with policy 4.2.0
  * Remove libfile-spec-perl, libtest-simple-perl, liburi-perl and
    libparams-validate-perl form build dependencies
  * Change homepage to metacpan (broken link)
  * Update description
  * Update debian/copyright format
  * Add source/format
  * Add upstream/metadata
  * Enable autopkgtest
  * Replace Jonas by /me in uploaders
  * Update debian/watch

 -- Xavier Guimard <x.guimard@free.fr>  Thu, 23 Aug 2018 22:04:09 +0200

libhtml-wikiconverter-oddmuse-perl (0.52-1) unstable; urgency=low

  * Initial official release. Closes: bug#448912.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 28 Jun 2008 23:40:06 +0200

libhtml-wikiconverter-oddmuse-perl (0.52-0~0jones1) jones; urgency=low

  * Initial unofficial release.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 28 Jun 2008 22:59:39 +0200
